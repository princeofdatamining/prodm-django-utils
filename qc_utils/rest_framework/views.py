from rest_framework.generics import *
from rest_framework.mixins import *

from .utils import REST_FRAMEWORK, load_serializer_class, HTTP_X_FILTER_VALUES
from .serializers import build_from_headers
from .error_messages import reset_error_messages
from qc_utils.python.objutils import getattr_recursive

# 使用 self 的 context 去序列化其他数据
def serialize_other(self, serialize_class, *args, **kwargs):
    kwargs.setdefault('context', self.get_serializer_context())
    return serialize_class(*args, **kwargs).data
GenericAPIView.serialize_other = serialize_other

LOGGED_PLACEHOLDER = ('0', '~')

class GenericAPIView(GenericAPIView):

    # view 也可以像 serializer／field 使用 fail
    # 但需要预先定义过 error_messages
    def fail(self, key, **kwargs):
        reset_error_messages(self)
        serializers.Field.fail(self, key, **kwargs)

    # 修改和获取的 serialize_class 可以不同
    # submit_serializer_class = None

    # set owner_is_logged after get_object
    owner_is_logged = False
    # object 跟 request.user 关联时，可以获取到额外的信息
    owner_serializer_class = None

    # 支持多格式的serializer_class
    serializer_classes = None
    SERIALIZER_CLASS = REST_FRAMEWORK.SERIALIZER_CLASS or 'serializer'

    def get_default_serializer_class(self):
        if self.owner_is_logged and self.owner_serializer_class:
            return self.owner_serializer_class
        klass = super(GenericAPIView, self).get_serializer_class()
        if isinstance(klass, str):
            return load_serializer_class(klass)
        return klass

    def get_method_serializer_class(self, by_method):
        default = self.get_default_serializer_class()
        # 修改资源时，请指定 submit_serializer_class
        # 为了防止利用自定义serializer来提交未经过validate的数据，请务必指定 
        if by_method and self.request.method in ['POST', 'PUT', 'PATCH']:
            return self.get_submit_serializer_class() or default
        # 在Headers中指定 X-Serializer-Fields: id, __str__, url
        klass = build_from_headers(self)
        if klass: return klass
        # 判断是否有指定的serializer
        mode = self.request.GET.get(self.SERIALIZER_CLASS)
        if not mode or not isinstance(self.serializer_classes, dict):
            return default
        return self.serializer_classes.get(mode) or default

    def get_serializer_class(self):
        return self.get_method_serializer_class(True)

    def get_submit_serializer_class(self):
        return self.submit_serializer_class

    CHANGE_SUBMIT_SERIALIZER = REST_FRAMEWORK.CHANGE_SUBMIT_SERIALIZER
    # CREATE/UPDATE 后返回跟 GET 一样的serialier结果
    def change_serializer_data(self, serializer):
        if not self.CHANGE_SUBMIT_SERIALIZER:
            return 
        serialize_class = self.get_method_serializer_class(False)
        serializer._data = self.serialize_other(serialize_class, serializer.instance)

    '''
        lookup_logged = True
            /api/user/ ==> always retrieve logged user
        lookup_logged_param = 'logged'
            /api/posts/ ==> list all posts
            /api/posts/?logged= ==> list posts of logged user
        lookup_logged_falsy_kwargs = (0, '0', '$')
            /api/users/`id` ==> retrieve lookup user
            /api/users/$ => retrieve logged user
    '''
    lookup_logged = False
    lookup_logged_param = '='
    lookup_logged_falsy_kwargs = ('~',)

    def get_logged_kwarg(self):
        return self.request.user.pk

    def prepare_lookup_kwarg(self, original=False):
        kwarg = self.lookup_url_kwarg or self.lookup_field
        if any([
            self.lookup_logged,
            self.lookup_logged_param in self.request.GET,
            self.kwargs.get(kwarg) in self.lookup_logged_falsy_kwargs,
        ]):
            value = self.get_logged_kwarg()
            return kwarg, value
        elif original:
            value = self.kwargs.get(kwarg)
            return None if value is None else kwarg, value
        return None, None

    def check_logged(self, *extra):
        logged = self.get_logged_kwarg()
        if not logged:
            return
        value = self.kwargs.get(self.lookup_url_kwarg or self.lookup_field)
        return any((
            self.lookup_logged,
            self.lookup_logged_param in self.request.GET,
            value in self.lookup_logged_falsy_kwargs,
            value in [logged, str(logged)],
            value in extra,
        ))

    def update_lookup_kwarg(self):
        kwarg, value = self.prepare_lookup_kwarg()
        if kwarg and value:
            self.kwargs[kwarg] = value

    # 判断 object 是否和 request.user 相关
    owner_fields = ()
    def check_owner_field(self, instance, field):
        if getattr_recursive(instance, field) == self.get_logged_kwarg():
            self.owner_is_logged = True
            return True

    def update_owner(self, instance):
        if not self.owner_fields:
            return
        if not isinstance(self.owner_fields, (list, tuple)):
            return self.check_owner_field(instance, self.owner_fields)
        else:
            for field in self.owner_fields:
                if self.check_owner_field(instance, field):
                    return True

    def get_object(self):
        # 如 /users/~ 时，更新 kwargs 用当前用户替换掉 ~
        self.update_lookup_kwarg()
        obj = super(GenericAPIView, self).get_object()
        # 更新 owner_is_logged，后续 get_serializer_class 时可利用
        self.update_owner(obj)
        return obj

    select_related_fields = ()
    def get_queryset(self):
        qs = super(GenericAPIView, self).get_queryset()
        #
        if self.select_related_fields:
            qs = qs.select_related(*self.select_related_fields)
        #
        qs = self.filter_header_queryset(qs)
        #
        kwarg, value = self.prepare_lookup_kwarg(original=True)
        if self.lookup_field and kwarg:
            qs = qs.filter(**{self.lookup_field: value})
        return qs

    header_field = None
    header_name = HTTP_X_FILTER_VALUES
    header_value_delim = ','
    def filter_header_queryset(self, qs):
        if not self.header_field or not self.header_name:
            return qs
        values = self.request.META.get(self.header_name, '')
        if not values:
            return qs
        qs = qs.filter(**{self.header_field+'__in':values.split(self.header_value_delim)})
        return qs
    #
#

class ListModelMixin(ListModelMixin):

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

class ListAPIView(ListModelMixin, GenericAPIView): pass

#
class CreateModelMixin(CreateModelMixin):

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        super(CreateModelMixin, self).perform_create(serializer)
        self.change_serializer_data(serializer)

class CreateAPIView(CreateModelMixin, GenericAPIView): pass
class ListCreateAPIView(ListModelMixin, CreateAPIView): pass

#

class RetrieveModelMixin(RetrieveModelMixin):

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

class _UpdateModelMixin(UpdateModelMixin):

    def perform_update(self, serializer):
        super(_UpdateModelMixin, self).perform_update(serializer)
        self.change_serializer_data(serializer)

class PatchModelMixin(_UpdateModelMixin):

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

class UpdateOnlyModelMixin(_UpdateModelMixin):

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

class UpdateModelMixin(PatchModelMixin):

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

#

class RetrieveAPIView(RetrieveModelMixin, GenericAPIView): pass
class PatchAPIView(PatchModelMixin, GenericAPIView): pass
class UpdateAPIView(UpdateModelMixin, GenericAPIView): pass
class UpdateOnlyAPIView(UpdateOnlyModelMixin, GenericAPIView): pass

class RetrievePatchAPIView(RetrieveModelMixin, PatchAPIView): pass
class RetrieveUpdateAPIView(RetrieveModelMixin, UpdateAPIView): pass

#

class DestroyModelMixin(DestroyModelMixin):

    # 软删除： 不调用 delete，改为设置删除标记
    soft_field = None
    destroy_value = True

    def perform_destroy(self, instance):
        if not self.soft_field:
            return super(DestroyModelMixin, self).perform_destroy(instance)
        setattr(instance, self.soft_field, self.destroy_value)
        instance.save()

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class HiddenModelMixin(DestroyModelMixin):

    soft_field = 'hidden'
    destroy_value = True

class DeletedModelMixin(DestroyModelMixin):

    soft_field = 'deleted'
    destroy_value = True
