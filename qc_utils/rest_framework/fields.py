from rest_framework.fields import *
from rest_framework.relations import *
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils import timezone
from django.core.files.base import ContentFile

import base64
from io import BytesIO

from .utils import REST_FRAMEWORK, reverse

# submit json 时，image/file 是BASE64过的文本串

def convert_base64(data, name):
    # data:image/jpeg;base64,.....
    meta, sep, binary = data.partition(';base64,')
    if not sep:
        return 
    binary = base64.b64decode(binary.encode('ascii'))
    content_type = meta.lstrip('data:')
    mime_type, sep, extension = content_type.partition('/')
    #
    return InMemoryUploadedFile(
        BytesIO(binary),
        name,
        timezone.now().strftime('%Y-%m-%d_%H%M%S_%f') + '.' + extension,
        content_type, len(binary), None, {}
    )

# 抛出 SkipField 可以不让该 field 设置到 validated_data 中
'''
        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            ...
            except SkipField:
                pass
            else:
                set_value(ret, field.source_attrs, validated_value)
'''
def raise_skip(*args, **kwargs):
    raise SkipField()

class Base64FileField(FileField):

    def to_internal_value(self, data):
        data = convert_base64(data, self.source)
        return super(Base64FileField, self).to_internal_value(data)

    raise_skip = False
    def run_validation(self, data=empty):
        if self.raise_skip:
            raise SkipField()
        return super(Base64FileField, self).run_validation(data)

class Base64ImageField(ImageField):

    def to_internal_value(self, data):
        data = convert_base64(data, self.source)
        return super(Base64ImageField, self).to_internal_value(data)

    raise_skip = False
    def run_validation(self, data=empty):
        if self.raise_skip:
            raise SkipField()
        return super(Base64ImageField, self).run_validation(data)

# 把 PrimaryKeyRelatedField 放入 dict 中去
class PKDictRelatedField(PrimaryKeyRelatedField):

    def __init__(self, **kwargs):
        self.key_name = kwargs.pop('key_name', 'id')
        super(PKDictRelatedField, self).__init__(**kwargs)

    def to_representation(self, value):
        value = super(PKDictRelatedField, self).to_representation(value)
        return None if value is None else {
            self.key_name: value,
        }

# 获取数据时，时间是否转为本地时间
class DateTimeField(DateTimeField):

    def __init__(self, *args, **kwargs):
        self.repr_localtime = kwargs.pop('localtime', None)
        super(DateTimeField, self).__init__(*args, **kwargs)

    def to_representation(self, value):
        if value:
            if self.repr_localtime is False:
                # 不去尝试 REST_FRAMEWORK.LOCAL_TIME
                pass
            elif self.repr_localtime or REST_FRAMEWORK.LOCAL_TIME:
                value = timezone.localtime(value)
        return super(DateTimeField, self).to_representation(value)

'''
ContentJsonField 只保持富文本的基本数据，显示交给客户端

content: [
    {"type":"text", "value": "..."}, # 文本
    {"type":"image", "value": "url"}, # 图片
    # type 可以扩展
]
'''
class ContentJsonField(JSONField):

    def to_internal_value(self, data):
        data = super(ContentJsonField, self).to_internal_value(data)
        # 自动转换
        if isinstance(data, str):
            data = dict(content=[dict(value=data)])
        elif isinstance(data, list):
            data = dict(content=data)
        # 判断类型和content
        if not isinstance(data, dict):
            self.fail('invalid')
        elif "content" not in data:
            self.fail('empty')
        elif not isinstance(data["content"], list):
            self.fail('invalid')
        # 只要含有有效 value 的集合
        data["content"] = [
            elem 
            for elem in data["content"]
            if isinstance(elem, dict) and elem.get('value')
        ]
        if not data["content"]:
            self.fail('empty')
        return data

    def to_representation(self, instance):
        data = super(ContentJsonField, self).to_representation(instance)
        return data

'''
    places = CommaCharField(delim=',')
    value:
        "长兴县,浙江,中国"
    to_representation:
        "places": [
            "长兴县",
            "浙江",
            "中国"
        ]
'''
class CommaCharField(CharField):

    def __init__(self, *args, **kwargs):
        self.delim = kwargs.pop('delim', None) or ','
        super(CommaCharField, self).__init__(*args, **kwargs)

    def to_internal_value(self, data):
        if isinstance(data, list):
            data = self.delim.join(data)
        return super(CommaCharField, self).to_internal_value(data)

    def to_representation(self, data):
        data = super(CommaCharField, self).to_representation(data)
        return data.split(self.delim) if data else []

# URL 无效时，使用默认URL
class FileField(FileField):

    def __init__(self, *args, **kwargs):
        self.default_url = kwargs.pop('default_url', None)
        super(FileField, self).__init__(self, *args, **kwargs)

    def to_representation(self, instance):
        url = super(FileField, self).to_representation(instance)
        return url or self.default_url

'''
    params: url 带上query string
    extra_params: 从obj取得属性，并填入 params
'''
class HyperlinkedIdentityField(HyperlinkedIdentityField):

    def __init__(self, *args, **kwargs):
        self.lookup = kwargs.pop('lookup', True)
        self.reverse_params = kwargs.pop('params', {})
        self.extra_params = kwargs.pop('extra_params', {})
        super(HyperlinkedIdentityField, self).__init__(*args, **kwargs)

    def get_reverse_kwargs(self, obj):
        kwargs = {}
        if self.lookup:
            kwargs[self.lookup_url_kwarg] = getattr(obj, self.lookup_field)
        return kwargs

    def get_reverse_params(self, obj):
        params = self.reverse_params.copy()
        for key, field in self.extra_params.items():
            params[key] = getattr(obj, field)
        return params

    def get_url(self, obj, view_name, request, format):
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk in (None, ''):
            return None
        kwargs = self.get_reverse_kwargs(obj)
        params = self.get_reverse_params(obj)
        return reverse(view_name, 
            params=params, reverse_func=self.reverse,
            kwargs=kwargs, request=request, format=format
        )
