from rest_framework import exceptions, status, renderers, settings
from rest_framework.views import exception_handler as rest_exception_handler

'''
### 默认的 400 错误正文

POST /api/auth/signin/
HTTP 400 Bad Request
Content-Type: application/json

{
    "username": [
        "请填写用户名。"
    ],
    "password": [
        "请填写密码。"
    ],
    "captcha": "验证码不正确。"
}

＝＝＝＝＝＝＝>

# 有时候只需要简单显示一条错误信息

POST /api/auth/signin/
HTTP/1.0 400 Bad Request
Content-Type: application/json

{
    "error_field": "captcha",
    "error_msg": "验证码不正确。"
}
'''

def get_single_error(errors):
    field = None
    if not errors:
        return '', field
    #
    if isinstance(errors, dict):
        if 'detail' in errors:
            errors = errors['detail']
        else:
            field = tuple(errors.keys())[0]
            errors = errors[field]
    #
    if isinstance(errors, list):
        errors = errors[0]
    #
    return errors, field

def exception_handler(exc, context):
    request = context['request']
    resp = rest_exception_handler(exc, context)
    # 只有返回JSON的情况下，才需要额外处理；
    # 不要影响 BrowsableAPIRenderer 的调试效果。
    if resp and isinstance(
        getattr(request, 'accepted_renderer', None),
        renderers.JSONRenderer
    ):
        errors, field = get_single_error(resp.data)
        resp.data = {
            'error_field': field,
            'error_msg': errors,
        }
    return resp
