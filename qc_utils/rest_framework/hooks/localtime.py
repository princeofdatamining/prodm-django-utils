from rest_framework.serializers import ModelSerializer
from ..fields import DateTimeField
from django.db import models
'''
根据设置 settings.REST_FRAMEWORK.LOCAL_TIME 来控制是否显示本地时间/UTC
'''
ModelSerializer.serializer_field_mapping[models.DateTimeField] = DateTimeField
