from rest_framework.serializers import ModelSerializer
from ..fields import PKDictRelatedField
'''
默认 ForeignKey 对象会被序列化成:
    attr: pk

指定过 Serializer 后序列化结果：
    attr: {
        id: pk,
        ...
    }

为了兼容，在未指定Serializer时，使序列化结果也表现为dict：
    attr: {
        id: pk,
    }
'''
ModelSerializer.serializer_related_field = PKDictRelatedField
