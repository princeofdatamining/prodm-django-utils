from rest_framework.serializers import BaseSerializer

from ..error_messages import reset_error_messages
'''
默认错误提示并不友好，如：
    此字段不能为空
    This field can not be null
需要定制
'''
method = 'is_valid'

saved = '_'+method
if not hasattr(BaseSerializer, saved):
    func = getattr(BaseSerializer, method)
    setattr(BaseSerializer, saved, func)
    def wrapper(self, *args, **kwargs):
        reset_error_messages(self)
        return func(self, *args, **kwargs)
    setattr(BaseSerializer, method, wrapper)
