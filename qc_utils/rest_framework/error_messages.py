from django.conf import settings
from django.utils.lru_cache import lru_cache
from rest_framework.settings import import_from_string
'''
# settings.py
REST_FRAMEWORK = {
    'ERROR_MESSAGES': 'app.path.to.filename.error_messages',
}

# app.path.to.filename.py
error_messages = {
    'rest_auth.restful.v1.SignInSerializer': {
        '.': {
            'user_not_exists': '该用户尚未注册。',
            'user_inactive': '此用户已被禁止。',
            'pwd_incorrect': '用户名或密码不正确。',
        },
        'username': {
            ('required', 'null', 'blank'): '请填写用户名。',
            'validators': {
            },
        },
        'password': {
            ('required', 'null', 'blank'): '请填写密码。',
        },
    },
}
'''
@lru_cache()
def load_error_messages():
    value = settings.REST_FRAMEWORK.get('ERROR_MESSAGES')
    if isinstance(value, str):
        value = import_from_string(value, 'ERROR_MESSAGES')
    while not isinstance(value, dict):
        if callable(value):
            value = value()
        else:
            value = dict()
    # 把 classname 转成 klass，便于后续判断 issubclass 等
    results = {}
    for classes, v in value.items():
        if not isinstance(classes, tuple):
            classes = (classes,)
        for klass in classes:
            results[import_from_string(klass, '')] = v
    return results

def get_class_name(given):
    if not isinstance(given, type):
        given = given.__class__
    return '{}.{}'.format(given.__module__, given.__name__)

def get_messages(given):
    messages = load_error_messages()
    for klass, v in messages.items():
        if given == klass or issubclass(given, klass):
            return v

def reset_error_messages(self):
    # 是否处理过
    if hasattr(self, '_error_messages_reset_done'):
        return 
    setattr(self, '_error_messages_reset_done', True)
    # 是否需要定制
    klass = self.__class__
    error_messages = get_messages(klass)
    if not error_messages:
        return 
    # 如果 error_messages 定义在类中，需要创建一个副本
    if hasattr(klass, 'error_messages') and id(klass.error_messages) == id(self.error_messages):
        self.error_messages = klass.error_messages.copy()
    #
    for name, messages in error_messages.items():
        if not name or name in ['.', 'self']:
            field = self
        else:
            field = self.fields.get(name)
        if not field:
            continue
        #
        for keys, message in messages.items():
            if not message:
                pass
            elif keys == 'validators':
                for validator in field.validators:
                    hints = message.get(get_class_name(validator))
                    if not hints:
                        continue
                    for attr, value in hints.items():
                        if not value:
                            pass
                        elif isinstance(attr, (tuple, list)):
                            for key in attr:
                                setattr(validator, key, value)
                        else:
                            setattr(validator, attr, value)
            elif isinstance(keys, (tuple, list)):
                for key in keys:
                    field.error_messages[key] = message
            else:
                field.error_messages[keys] = message
