from .base import APITestCase
import django
from ..utils import *

class TestUtils(APITestCase):

    def setUp(self):
        super(TestUtils, self).setUp()
        SERIALIZER_CLASSES['Blah'] = 'django.VERSION'
        SERIALIZER_CLASSES['Fail'] = '!@#$%^&*()'
        self.reverse_func = reverse

    def test_load_serializer(self):
        notexists = load_serializer_class('notexists')
        self.assertEqual(None, notexists)
        #
        blah = load_serializer_class('Blah')
        self.assertEqual(django.VERSION, blah)
        # ValueError: Empty module name
        with self.assertRaises(ValueError):
            load_serializer_class('Fail')
        #

    def test_reverse(self):
        url = self.reverse_api('sign-in', params=dict(key='value'))
        self.assertIn('?key=value', url)
