from .base import APITestCase
from ..serializers import *
from django.conf import settings
from rest_auth.restful.v1 import ProfileView
from rest_framework.test import APIRequestFactory
from qc_utils.python.objutils import DictObject

base64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAQAAAADpb+tAAAAJUlEQVQIW2NgQAf/MUSgghgS/9FoFJUYglhVUlsQxMXh1v+YggDKvxfpwg9anQAAAABJRU5ErkJggg=='

class TestSerializer(APITestCase):

    def setUp(self):
        super(TestSerializer, self).setUp()
        self.u01 = self.force_user('user01')
        # settings.REST_FRAMEWORK['SERIALIZER_CLASSES']['User'] = 'rest_auth.restful.v1.UserPublic'

    def test_build(self):
        view = ProfileView()
        view.request = request = APIRequestFactory().request()
        fields = 'id,stringify'
        request.META['HTTP_X_SERIALIZER_FIELDS'] = fields
        serializer = build_from_headers(view)
        data = serializer(self.u01).data
        self.assertIsInstance(data, dict)
        self.assertEqual(2, len(data))
        self.assertIn('id', data)
        self.assertIn('stringify', data)

    def test_build_deep(self):
        'upload_media' in settings.INSTALLED_APPS and self._test_upload()
        'json_comment' in settings.INSTALLED_APPS and self._test_comment()
    def _test_result(self, view, instance, fields, user_fields):
        view = view()
        view.request = request = APIRequestFactory().request()
        request.META['HTTP_X_SERIALIZER_FIELDS'] = fields
        request.META['HTTP_X_SERIALIZER_USER_FIELDS'] = user_fields
        serializer = build_from_headers(view)
        return serializer(instance).data
    def _test_comment(self):
        from json_comment.models import Comment
        from json_comment.restful.v1 import CommentView, Serializer
        instance = Comment(user=self.u01, title='hello')
        data = self._test_result(CommentView, instance, 'user@User,title', 'id,stringify')
        self.assertIn('title', data)
        self.assertEqual(2, len(data))
        self.assertIn('stringify', data['user'])
        self.assertIn('id', data['user'])
        self.assertEqual(2, len(data['user']))
    def _test_upload(self):
        from upload_media.models import Upload
        from upload_media.restful.v1 import UploadsView, UploadSerializer
        instance = Upload(user=self.u01, filename='file.name')
        data = self._test_result(UploadsView, instance, 'user@User,filename', 'id,stringify')
        self.assertEqual(2, len(data))
        self.assertIn('filename', data)
        self.assertEqual(2, len(data['user']))
        self.assertIn('stringify', data['user'])
        self.assertIn('id', data['user'])

    ###

    def test_model(self):
        'json_comment' in settings.INSTALLED_APPS and self._test_post_comment()
        'upload_media' in settings.INSTALLED_APPS and self._test_post_upload()
    def _test_post_comment(self):
        from json_comment.restful.v1 import SubmitSerializer as Serializer
        Serializer.USER_FIELDS = ''
        Serializer.URL_KWARGS = dict(user_id='uid')
        s = Serializer(data=dict(content=[
            {"value":"text"},
        ]), context=self.build_context())
        s.is_valid()
        data = s.save()
        self.assertEqual(self.u01.pk, data.user_id)
    def _test_post_upload(self):
        from upload_media.restful.v1 import UploadSubmitSerializer as Serializer
        s = Serializer(data=dict(base64=base64), context=self.build_context())
        s.is_valid()
        data = s.save()
        self.assertEqual(self.u01.pk, data.user_id)
    def build_context(self):
        request = APIRequestFactory().request()
        request.user = self.u01
        view = DictObject(
            kwargs=dict(uid=self.u01.pk),
            content_type_id=1,
            object_id=1,
            category='unclassed',
        )
        return dict(request=request, view=view)
