from .base import APITestCase
from ..fields import *
from django.conf import settings
from django.utils import timezone
from qc_utils.python.objutils import DictObject
from django.test.client import encode_multipart, RequestFactory
from rest_framework.test import APIRequestFactory
from rest_framework.settings import api_settings

class TestFields(APITestCase):

    def setUp(self):
        super(TestFields, self).setUp()
        self.request = request = APIRequestFactory().request()
        request.versioning_scheme = api_settings.DEFAULT_VERSIONING_CLASS()
        request.version = request.GET.get('version', api_settings.DEFAULT_VERSION)

    def test_base64(self):
        # test in upload app
        pass

    def test_repr_pk_related_in_dict(self):
        f = PKDictRelatedField(key_name='uuid', read_only=True)
        r = f.to_representation(DictObject(pk=100))
        self.assertIsInstance(r, dict)
        self.assertEqual(1, len(r))
        self.assertEqual(100, r['uuid'])

    def test_repr_localtime(self):
        now = timezone.now()
        vf = DateTimeField(localtime=False).to_representation(now)
        vg = DateTimeField().to_representation(now)
        vt = DateTimeField(localtime=True).to_representation(now)
        self.assertNotEqual(vt, vf)
        if all((
            'qc_utils.rest_framework.hooks.localtime' in settings.IMPORT_HOOKS,
            settings.REST_FRAMEWORK['LOCAL_TIME'],
        )):
            self.assertEqual(vt, vg)
        else:
            self.assertEqual(vf, vg)

    def test_json_content(self):
        # test in comment app
        pass

    def test_comma(self):
        f = CommaCharField(delim='|')
        s = 'a|b|c'
        a = ['a', 'b', 'c']
        self.assertEqual(s, f.to_internal_value(a))
        self.assertEqual(a, f.to_representation(s))

    def check_file_field(self, want, data, *args, **kwargs):
        f = FileField(*args, **kwargs)
        # f._context = dict(request=self.request)
        self.assertEqual(want, f.to_representation(data))
    def test_default_url(self):
        default_url = 'http://www.python.org/'
        self.check_file_field(None, None)
        self.check_file_field(None, DictObject())
        self.check_file_field(None, DictObject(url=''))
        #
        self.check_file_field(default_url, None, default_url=default_url)
        self.check_file_field(default_url, DictObject(), default_url=default_url)
        self.check_file_field(default_url, DictObject(url=''), default_url=default_url)
        #

    def new_hyperlink_field(self, *args, **kwargs):
        f = HyperlinkedIdentityField(*args, **kwargs)
        f._context = dict(request=self.request)
        return f
    def test_hyperlinked(self):
        data = DictObject(username='a')
        #
        f = self.new_hyperlink_field('sign-in')
        with self.assertRaises(AttributeError):
            url = f.to_representation(data)
        #
        f = self.new_hyperlink_field('sign-in', lookup=False)
        url = f.to_representation(data)
        #
        f = self.new_hyperlink_field('sign-in', lookup=False, params=dict(on=1))
        url = f.to_representation(data)
        self.assertIn('?on=1', url)
        #
        f = self.new_hyperlink_field('sign-in', lookup=False, params=dict(on=0), extra_params=dict(name='username'))
        url = f.to_representation(data)
        self.assertIn('on=0', url)
        self.assertIn('name='+data.username, url)
