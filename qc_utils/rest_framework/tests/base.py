from rest_framework.test import APITestCase
from rest_framework.settings import api_settings
from rest_framework.reverse import reverse
from ..utils import reverse

from django.conf import settings
from django.utils.translation import activate

from qc_utils.django.tests.base import AuthMixin

class APITestCase(AuthMixin, APITestCase):

    LANGUAGE = ''
    default_format = 'json'

    def setUp(self):
        activate(self.LANGUAGE or settings.LANGUAGE_CODE)
        self.reverse_func = reverse

    def assert400reason(self, reason, data, field=None):
        if isinstance(data, dict):
            data = data[field]
        if isinstance(reason, (tuple, list)):
            for r in reason:
                if str(r) in data:
                    break
            else:
                self.assertEqual(reason, data)
        else:
            self.assertIn(str(reason), data)

    def reverse(self, viewname, *args, api=False, **kwargs):
        if api: viewname = self.make_api_viewname(viewname)
        return self.reverse_func(viewname, *args, **kwargs)

    def reverse_api(self, viewname, *args, **kwargs):
        kwargs.setdefault('api', True)
        return self.reverse(viewname, *args, **kwargs)

    def make_api_viewname(self, viewname):
        ver = api_settings.DEFAULT_VERSION
        return '{}:{}'.format(ver, viewname)

    def request_kwargs(self, user=None):
        kwargs = {}
        if user:
            kwargs['HTTP_AUTHORIZATION'] = 'token {}'.format(user.token)
        return kwargs

    def assertResults(self, resp, pks, results='results', key='id'):
        self.assertEqual(200, resp.status_code)
        self.assertIn(results, resp.data)
        results = resp.data.get(results)
        for row in results:
            self.assertIn(row.get(key), pks)
        self.assertEqual(len(pks), len(results))
