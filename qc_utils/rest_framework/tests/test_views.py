from .base import APITestCase
from rest_auth.restful.v1 import *
from ..views import *
from ..serializers import build_serializer
from ..utils import SERIALIZER_CLASSES

class TestViews(APITestCase):

    def setUp(self):
        super(TestViews, self).setUp()
        self.url = self.reverse_api('info-or-profile', kwargs=dict(pk='~'))
        self.users = self.reverse_api('user-info-list')
        self.u01 = self.force_user('user01')
        self.u02 = self.force_user('user02')
        ProfileView.lookup_logged_param = UsersView.lookup_logged_param = 'mine'
        self.lookup_logged, ProfileView.lookup_logged = ProfileView.lookup_logged, False
        self.filter_logged, UsersView.lookup_logged = UsersView.lookup_logged, False

    def tearDown(self):
        ProfileView.lookup_logged = self.lookup_logged
        UsersView.lookup_logged = self.filter_logged
        ProfileView.lookup_logged_param = UsersView.lookup_logged_param = ''

    def curl(self, *args, who=None, u=None, method='get', url='', params='', **kwargs):
        if u: kwargs['HTTP_AUTHORIZATION'] = 'token '+u.token
        url = url or self.url
        if who: url = url.replace('~', str(who.pk))
        return getattr(self.client, method)(url + params, *args, **kwargs)

    def check_user(self, resp, keys):
        self.assertEqual(200, resp.status_code)
        keys = keys.split(',')
        for key in keys:
            self.assertIn(key, resp.data)
        self.assertEqual(len(keys), len(resp.data))

    def test_origin(self):
        ProfileView.serializer_class = build_serializer(UserPublic, 'username')
        self.check_user(self.curl(who=self.u02), 'username')

    def test_import(self):
        ProfileView.owner_serializer_class = None
        ProfileView.serializer_class = 'TestViewsImport'
        SERIALIZER_CLASSES['TestViewsImport'] = 'rest_auth.restful.v1.UserSerializer'
        self.check_user(self.curl(who=self.u02), 'id,username,stringify,email,first_name,last_name')

    def test_classes(self):
        ProfileView.serializer_classes = dict(
            id=build_serializer(UserPublic, 'id'),
        )
        self.check_user(self.curl(who=self.u02, params='?serializer=id'), 'id')

    def test_headers(self):
        self.check_user(self.curl(who=self.u02, HTTP_X_SERIALIZER_FIELDS='stringify'), 'stringify')

    def test_logged(self):
        ProfileView.owner_serializer_class = build_serializer(UserSerializer, 'id', 'first_name', 'last_name')
        self.check_user(self.curl(who=self.u02, u=self.u02), 'id,first_name,last_name')

    #####
    
    def check_users(self, n, resp):
        self.assertEqual(200, resp.status_code)
        self.assertEqual(n, len(resp.data['results']))

    def test_filter_logged(self):
        self.check_users(2, self.curl(url=self.users))
        UsersView.lookup_logged = True
        self.check_users(0, self.curl(url=self.users))
        self.check_users(1, self.curl(url=self.users, u=self.u01))

    def test_filter_logged_param(self):
        self.check_users(2, self.curl(url=self.users))
        self.check_users(0, self.curl(url=self.users, params='?mine='))
        self.check_users(1, self.curl(url=self.users, params='?mine=', u=self.u01))

    def test_filter_logged_kwargs(self):
        self.check_users(2, self.curl(url=self.users))

    #####

    def test_lookup_logged_falsy_kwargs(self):
        resp = self.curl()
        self.assertEqual(404, resp.status_code)
        #
        resp = self.curl(u=self.u01)
        self.assertEqual(200, resp.status_code)
        self.assertEqual(self.u01.pk, resp.data['id'])

    def test_header_filter(self):
        # http <url> X-Filter-Values:v1,v2
        pass
