from .base import APITestCase
from ..permissions import *
from rest_auth.restful.v1 import ProfileView

class TestPermission(APITestCase):

    def setUp(self):
        super(TestPermission, self).setUp()
        self.url = self.reverse_api('info-or-profile', kwargs=dict(pk='~'))
        self.u01 = self.force_user('user01')
        self.u02 = self.force_user('user02')

    def curl(self, *args, who=None, u=None, method='get', **kwargs):
        if u: kwargs['HTTP_AUTHORIZATION'] = 'token '+u.token
        url = self.url.replace('~', str(who.pk)) if who else self.url
        return getattr(self.client, method)(url, *args, **kwargs)
        self.assertEqual(200, resp.status_code)

    def test_readonly(self):
        # 只读
        ProfileView.permission_classes = (IsAuthenticatedOrReadOnly,)
        resp = self.curl(who=self.u01)
        self.assertEqual(200, resp.status_code)

    def test_unauthenticate(self):
        # 需要登录
        ProfileView.permission_classes = (IsAuthenticated,)
        resp = self.curl(who=self.u01)
        self.assertEqual(401, resp.status_code)

    def test_permission_denied(self):
        # 已登陆用户为owner方可访问
        ProfileView.permission_classes = (make_owner_permission('pk', readonly=False),)
        resp = self.curl(who=self.u01, u=self.u02)
        self.assertEqual(403, resp.status_code)

    #####

    def test_write_fail_401(self):
        # 未登录不可写
        ProfileView.permission_classes = (IsAuthenticatedOrReadOnly,)
        resp = self.curl({
            'first_name': 'haha',
        }, who=self.u01, method='patch')
        self.assertEqual(401, resp.status_code)

    def test_write_fail_403(self):
        # 非owner不可写
        ProfileView.permission_classes = (make_owner_permission('pk'),)
        resp = self.curl({
            'first_name': 'haha',
        }, who=self.u01, u=self.u02, method='patch')
        self.assertEqual(403, resp.status_code)

    def test_write_succ(self):
        # owner可写
        ProfileView.permission_classes = (make_owner_permission('pk'),)
        resp = self.curl({
            'first_name': 'haha',
        }, who=self.u01, u=self.u01, method='patch')
        self.assertEqual(200, resp.status_code)
