from rest_framework.permissions import *
from .utils import REST_FRAMEWORK
from qc_utils.python.objutils import getattr_recursive

def check_readonly(request):
    return request.method in SAFE_METHODS

# 开启设置后，需要登录的地方会弹出指定错误
if REST_FRAMEWORK.RAISE_UNAUTHENTICATED:

    def check_authenticated(ret):
        if not ret:
            pass
        return ret

    class IsAuthenticated(IsAuthenticated):

        def has_permission(self, request, view):
            return check_authenticated(super(IsAuthenticated, self).has_permission(request, view))

    class IsAuthenticatedOrReadonly(IsAuthenticatedOrReadonly):

        def has_permission(self, request, view):
            return check_authenticated(super(IsAuthenticatedOrReadonly, self).has_permission(request, view))

def make_owner_permission(*owner_fields, readonly=True, func=None):

    '''
        跟object相关的用户才可以｀访问并修改｀资源；
        readonly＝False：非相关用户不能进行任何操作；
        readonly＝True：非相关用户只能`访问`资源；
        ＃
    owner_fields：
        Comment->Post->User
        ‘post.user.pk’可以限制评论只可被贴子的作者置顶
        #
        为什么是＊owner_fields？
        跟订单相关的用户可以是买家，也可以是卖家
    '''
    class OwnerPermission(IsAuthenticated):

        def has_permission(self, request, view):
            if readonly and check_readonly(request):
                return True
            return super(OwnerPermission, self).has_permission(request, view)

        def has_object_permission(self, request, view, obj):
            if callable(func):
                ret = func(request, view, obj)
                if isinstance(ret, bool):
                    return ret
            if readonly and check_readonly(request):
                return True
            pk = request.user.pk
            for owner_field in owner_fields:
                attr = getattr_recursive(obj, owner_field)
                if attr == pk or pk == getattr(attr, 'pk', None):
                    return True
            return False

    return OwnerPermission
