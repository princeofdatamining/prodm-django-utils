from rest_framework.serializers import *

from .fields import PKDictRelatedField
from .utils import load_serializer_class

# 加上 __str__ 的别名 stringify
stringify = CharField(source='__str__', read_only=True)
Serializer._declared_fields['stringify'] = stringify
ModelSerializer._declared_fields['stringify'] = stringify

''' 把 PrimaryKeyRelatedField 放入 dict 中去
    默认的ForeignKey是这样：
        "user": 2
    而指定了Serializer是这样：
        "user": {
            "id": 2,
            ...
        }
    两种格式不兼容
'''
ModelSerializer.serializer_related_field = PKDictRelatedField

# 使用 self 的 context 去序列化其他数据
def serialize_other(self, serialize_class, *args, **kwargs):
    kwargs.setdefault('context', self.context)
    serializer = serialize_class(*args, **kwargs)
    serializer.bind('$', self) # bind才会有parent
    return serializer.data
BaseSerializer.serialize_other = serialize_other

### 让 Serializer 支持 captcha ###

BaseSerializer.CAPTCHA = False
BaseSerializer.CAPTCHA_HASHKEY = 'captcha_key'
BaseSerializer.CAPTCHA_ANSWER = 'captcha_val'
def validate_captcha(self, data):
    if not getattr(self, 'CAPTCHA', False):
        return 
    hashkey = data.get(self.CAPTCHA_HASHKEY)
    answer = data.get(self.CAPTCHA_ANSWER)
    from qc_utils.packages.captcha import validate_captcha
    try:
        validate_captcha(hashkey, answer, ValidationError)
    except:
        raise ValidationError({'captcha': self.error_messages['captcha']})

class Serializer(Serializer):

    validate_captcha = validate_captcha
    def to_internal_value(self, data):
        self.validate_captcha(data)
        return super(Serializer, self).to_internal_value(data)

class ModelSerializer(ModelSerializer):

    validate_captcha = validate_captcha
    def to_internal_value(self, data):
        self.validate_captcha(data)
        return super(ModelSerializer, self).to_internal_value(data)

    # 创建时，自动填当前用户信息
    # 比如发表文章时，自动设置作者为当前用户
    USER_FIELDS = ''
    # 创建时，自动从路径参数中读取病填写
    # 比如 url: /resources/`pk`
    # 给此资源创建关联数据时，可以直接设置外键为`pk`
    URL_KWARGS = {}

    def save(self, **kwargs):
        #
        if self.USER_FIELDS:
            if not isinstance(self.USER_FIELDS, (list, tuple)):
                self.__class__.USER_FIELDS = self.USER_FIELDS.split(',')
            for f in self.USER_FIELDS:
                kwargs.setdefault(f, self.context['request'].user.pk)
        #
        for f, key in self.URL_KWARGS.items():
            kwargs.setdefault(f, self.context['view'].kwargs[key])
        #
        return super(ModelSerializer, self).save(**kwargs)

# 从基类派生出指定的 Serializer
def build_serializer(Base, *includes, **declared):

    class BuildSerializer(Base):
    
        class Meta(Base.Meta):
            fields = tuple(filter(Base.Meta.fields.__contains__, includes))

    for field_name, field in declared.items():
        if field:
            BuildSerializer._declared_fields[field_name] = field

    return BuildSerializer

# 构建 Serializer 中的 Serializer
def build_child_serializer(View, name):
    base = load_serializer_class(name)
    if not base:
        return 
    fields = View.request.META.get('HTTP_X_SERIALIZER_{}_FIELDS'.format(name.upper()))
    if not fields:
        return base()
    return build_serializer(base, *fields.split(','))()

''' 根据 Headers 中的 X-SERIALIZER-FIELDS 派生出指定的  Serializer
    GET ... X-Serializer-Fields:id,user X-Serializer-User-Fields:id,stringify,url
    HTTP/1.0 200 OK
    {
        "id": 1,
        "user": {
            "id": 2
        }
    }

    GET ... X-Serializer-Fields:id,user@UserX X-Serializer-User-Fields:id,stringify,url
    HTTP/1.0 200 OK
    {
        "id": 1,
        "user": {
            "id": 2
        }
    }

    GET ... X-Serializer-Fields:id,user@User X-Serializer-UserX-Fields:id,stringify,url
    HTTP/1.0 200 OK
    {
        "id": 1, 
        "user": {
            "id": 2, 
            "stringify": "", 
            "url": "", 
            "username": ""
        }
    }

    GET ... X-Serializer-Fields:id,user@User X-Serializer-User-Fields:id,stringify
    HTTP/1.0 200 OK
    {
        "id": 1, 
        "user": {
            "id": 2, 
            "stringify": ""
        }
    }
'''
def build_from_headers(View):
    fields = View.request.META.get('HTTP_X_SERIALIZER_FIELDS')
    if not fields:
        return
    if hasattr(View, 'get_default_serializer_class'):
        base = View.get_default_serializer_class()
    else:
        base = View.get_serializer_class()
    includes, declared = [], {}
    for f in fields.split(','):
        fn, _, sn = f.partition('@')
        includes.append(fn)
        sn and declared.setdefault(fn, build_child_serializer(View, sn))
    return build_serializer(base, *includes, **declared)
