from django.conf import settings
from django.utils.lru_cache import lru_cache
from rest_framework.settings import perform_import
from rest_framework.reverse import reverse as _reverse

from urllib.parse import urlencode

HTTP_X_SERIALIZER_FIELDS = 'HTTP_X_SERIALIZER_FIELDS'
HTTP_X_FILTER_VALUES = 'HTTP_X_FILTER_VALUES'

from qc_utils.django.setting_utils import Settings
REST_FRAMEWORK = Settings('REST_FRAMEWORK')
SERIALIZER_CLASSES = REST_FRAMEWORK.SERIALIZER_CLASSES or {}

@lru_cache()
def load_serializer_class(model_name):
    serializer_class = SERIALIZER_CLASSES.get(model_name)
    if not serializer_class:
        return
    return perform_import(serializer_class, 'SERIALIZER_CLASSES.{}'.format(model_name))

def reverse(*args, params=None, reverse_func=None, **kwargs):
    url = (reverse_func or _reverse)(*args, **kwargs)
    if params:
        extra = urlencode(params)
        url += ('&' if '?' in url else '?') + extra
    return url
