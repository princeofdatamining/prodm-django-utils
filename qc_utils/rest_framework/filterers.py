import django_filters

'''
class CopyFilter(SimpleDjangoFilter):

    lookup_choices = (
        (['t', 'y', '1', 'true', 'yes'], dict(value='', exclude=True)),
        (['f', 'n', '0', 'false', 'no'], dict(value='')),
    )

class ArticleFilterSet(django_filters.FilterSet):

    ref_to = CopyFilter(name='ref_id')

?ref_to=1 ==> exclude(ref_id__exact='')
?ref_to=0 ==> filter(ref_id__exact='')
'''

invalid = object()

class SimpleDjangoFilter(django_filters.Filter):

    def lookups(self):
        if hasattr(self, 'lookup_choices'):
            return self.lookup_choices
        raise NotImplementedError(
            'The SimpleDjangoFilter.lookups() method must be overridden to '
            'return a list of tuples (value, verbose value)')

    def value(self, value):
        for params, conf in self.lookups():
            if value == params or value in params:
                setattr(self, 'exclude', conf.get('exclude', False))
                'lookup' in conf and setattr(self, 'lookup_expr', conf['lookup'])
                return conf.get('value')
        return invalid

    def filter(self, qs, value):
        value = self.value(value)
        if value is invalid:
            return qs
        if self.distinct:
            qs = qs.distinct()
        qs = self.get_method(qs)(**{'%s__%s' % (self.name, self.lookup_expr): value})
        return qs
