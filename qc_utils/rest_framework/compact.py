import rest_framework.serializers
import rest_framework.fields

# mysql + django-jsonfield 时， ModelSerializer 没有处理
try:
    from jsonfield.fields import JSONField
except:
    pass
else:
    rest_framework.serializers.ModelSerializer.serializer_field_mapping[JSONField] = rest_framework.fields.JSONField
