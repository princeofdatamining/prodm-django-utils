
_default = object()

def dict_like(data):
    if isinstance(data, dict):
        return True
    try:
        dict(data)
    except:
        return False
    return True

def items_generator(data):
    if isinstance(data, dict):
        for k, v in data.items():
            yield k, v
    else:
        for k, v in data:
            yield k, v

def get_value(data, key, default=None):
    if isinstance(data, dict):
        return data.get(key, default)
    for k, v in items_generator(data):
        if k == key:
            return v
    return default

def set_value(data, key, value):
    if isinstance(data, dict):
        data[key] = value
        return 
    for i, (k, v) in enumerate(data):
        if k == key:
            data[i] = (k, value)
            return 
    data.append((key, value))

def in_dict(data, key):
    return get_value(data, key, _default) is not _default

def update_recursive(data, extra):
    if not dict_like(extra):
        return data
    for k, v in items_generator(extra):
        older = get_value(data, k, _default)
        if older is _default or not dict_like(v):
            set_value(data, k, v)
            continue
        if not dict_like(older):
            set_value(data, k, v)
        else:
            update_recursive(older, v)
