from unittest import TestCase
import logging

from .objutils import *

class TestDummyObject(TestCase):

    def setUp(self):
        self.v = DummyObject(name='Alice', age=13)

    def test(self):
        self.assertEqual(self.v.name, 'Alice')
        self.assertEqual(self.v.age, 13)
        with self.assertRaises(AttributeError):
            _ = self.v.unknown

class TestDictObject(TestCase):

    def setUp(self):
        self.v = DictObject(name='Alice', age=13)
        self.v['slug-field'] = 'A-1'

    def test(self):
        self.assertEqual(self.v.name, 'Alice')
        self.assertEqual(self.v.age, 13)
        self.v.age = 14
        self.assertEqual(self.v.age, 14)
        self.v.gender = 'Male'
        self.assertEqual(self.v.gender, 'Male')
        self.assertEqual(self.v['slug-field'], 'A-1')
        with self.assertRaises(AttributeError):
            _ = self.v.unknown

class TestRecursiveAttr(TestCase):

    def setUp(self):
        parent = DummyObject(name='Bob', mobile='911')
        self.girl = DummyObject(name='Alice', parent=parent)
        
    def test(self):
        self.assertEqual(getattr_recursive(self.girl, 'name'), 'Alice')
        self.assertEqual(getattr_recursive(self.girl, 'unknown'), None)
        self.assertEqual(getattr_recursive(self.girl, 'parent.mobile'), '911')
        self.assertEqual(getattr_recursive(self.girl, 'parent.unknown'), None)
        #
        setattr_recursive(self.girl, 'name', 'Cindy')
        setattr_recursive(self.girl, 'age', 13)
        setattr_recursive(self.girl, 'parent.mobile', '110')
        setattr_recursive(self.girl, 'parent.age', 42)
        #
        self.assertEqual(getattr_recursive(self.girl, 'name'), 'Cindy')
        self.assertEqual(getattr_recursive(self.girl, 'age'), 13)
        self.assertEqual(getattr_recursive(self.girl, 'parent.mobile'), '110')
        self.assertEqual(getattr_recursive(self.girl, 'parent.age'), 42)

class TestCachedFunction(TestCase):

    def foo(self, n):
        self.logger.info('cached lazy function')
        return list(range(n))

    def setUp(self):
        self.logger = logging.getLogger('cached_function')
        self.Foo = CachedFunction(self.foo, 5)

    def test(self):
        list, temp = object(), object()
        with self.assertLogs('cached_function', level='INFO') as cm:
            list = self.Foo()
        self.assertEqual(cm.output, ['INFO:cached_function:cached lazy function'])
        #
        with self.assertLogs('cached_function', level='INFO') as cm:
            self.logger.info('')
            temp = self.Foo()
        self.assertEqual(cm.output, ['INFO:cached_function:'])
        self.assertIs(list, temp)
