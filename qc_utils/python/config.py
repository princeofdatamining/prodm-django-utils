import os
from collections import OrderedDict
from .objutils import DictObject
from .container import update_recursive

supported_parsers = []

def escape_str(s):
    if not s: return s
    s = s.strip()
    if s.startswith('"') and s.endswith('"'): return s[1:-1]
    return s

def text_parser(stream):
    data = DictObject()
    for line in stream.readlines():
        line = line.strip()
        if not line or line[0] in ['#']:
            continue
        key, sep, val = line.partition(':')
        key, val = escape_str(key), escape_str(val)
        key and sep and data.setdefault(key, val)
    return data

def json_parser(stream):
    return json.load(stream, object_pairs_hook=DictObject)

def yaml_parser(stream):
    return yaml.safe_load(stream)

try:
    import json
except:
    pass
else:
    supported_parsers.append(('.json', json_parser))

try:
    import yaml
except:
    pass
else:
    supported_parsers.append(('.yaml', yaml_parser))

def register_simple(extension='.txt'):
    supported_parsers.append((extension, text_parser))

def load_stream(stream, match=None):
    for (extension, parser) in supported_parsers:
        if match:
            if callable(match):
                if not match(extension):
                    continue
            else:
                if extension != match:
                    continue
        stream.seek(0, 0)
        try:
            return parser(stream)
        except:
            pass

def load_file(file, **kwargs):
    if not os.path.isfile(file):
        return
    with open(file, encoding='utf-8') as fp:
        return load_stream(fp, match=file.endswith)

def load_config(*args, **kwargs):
    data = DictObject()
    for f in args:
        temp = (load_file if isinstance(f, str) else load_stream)(f, **kwargs) 
        temp and update_recursive(data, temp)
    return data
