from unittest import TestCase
import io

from .config import *
from .container import *

bJson = b'{\n' + b'\n'.join([
    b'"name": "Alice",',
    b'"age": 13,',
    b'"unicode": "\xe4\xbd\xa0\xe5\xa5\xbd"',
]) + b'}'

bText = b'\n'.join([
    b'name : Alice',
    b'age: "13"',
    b'#comment: .....',
    b'unicode: "\xe4\xbd\xa0\xe5\xa5\xbd"',
])

bYaml = b'\n'.join([
    b'name: Alice',
    b'age: 13',
    b'#comment: .....',
    b'unicode: "\xe4\xbd\xa0\xe5\xa5\xbd"',
])

class BaseConfig(TestCase):

    def binary_to_stream(self, bytes):
        bs = io.BytesIO(bytes)
        bs.seek(0, 0)
        text = bs.read().decode('utf-8')
        us = io.StringIO(text)
        return us

    def setUp(self):
        self.text = self.binary_to_stream(bText)
        self.json = self.binary_to_stream(bJson)
        self.yaml = self.binary_to_stream(bYaml)

class TestParser(BaseConfig):

    def check_base(self, data):
        self.assertTrue(dict_like(data))
        self.assertEqual("Alice", get_value(data, 'name'))
        self.assertEqual("你好", get_value(data, 'unicode'))

    def check_text(self, data):
        self.check_base(data)
        self.assertEqual("13", get_value(data, 'age'))

    def check_data(self, data):
        self.check_base(data)
        self.assertEqual(13, get_value(data, 'age'))

    def test_text(self):
        self.check_text(text_parser(self.text))

    def test_json(self):
        self.check_data(json_parser(self.json))

    def test_yaml(self):
        self.check_data(yaml_parser(self.yaml))

YamlSample = '\n'.join([
    'NAME: "<name>"',
    'DICT_TO_VAR:',
    '   DUMMY:',
    'VAR_TO_DICT: VAR',
    'DATABASE:',
    '   HOST: localhost',
    '   PASSWORD: ',
    '   OPTIONS:',
    '       CHARSET: utf8',
])
YamlCustom = '\n'.join([
    'NAME: "YAML"',
    'DICT_TO_VAR: VAR',
    'VAR_TO_DICT:',
    '   DUMMY: true',
    'DATABASE:',
    '   HOST: ""',
    '   PASSWORD: "***"',
    '   OPTIONS:',
    '       CHARSET: utf8mb4',
    '       COLLATE: utf8mb4_general_ci',
])
class TestConfig(TestCase):

    def check_conf(self, conf):
        self.assertTrue("YAML", conf.NAME)
        self.assertTrue("VAR", conf.DICT_TO_VAR)
        VAR_TO_DICT = conf.VAR_TO_DICT
        self.assertTrue(dict_like(VAR_TO_DICT))
        self.assertTrue(get_value(VAR_TO_DICT, "DUMMY"))
        db = conf.DATABASE
        self.assertTrue(dict_like(db))
        self.assertEqual('', get_value(db, 'HOST'))
        self.assertEqual('***', get_value(db, 'PASSWORD'))
        options = get_value(db, 'OPTIONS')
        self.assertTrue(dict_like(options))
        self.assertEqual('utf8mb4', get_value(options, 'CHARSET'))
        self.assertEqual('utf8mb4_general_ci', get_value(options, 'COLLATE'))

    def test_yaml(self):
        conf = load_config(io.StringIO(YamlSample), io.StringIO(YamlCustom), match='.yaml')
        self.check_conf(conf)

    def test_json(self):
        pass
