from unittest import TestCase
import logging

from .container import *

class BaseDictTest(TestCase):

    def setUp(self):
        self.base = dict(name='Alice', age=13)
        self.misc = [('name','Alice'), ('age', 13)]

    def get_keys(self, d):
        return {k for (k, _) in items_generator(d)}

    def check_get(self, d, key, base):
        self.assertEqual(base, get_value(d, key))

class TestDictLike(BaseDictTest):

    def test_like(self):
        self.assertTrue(dict_like(self.base))
        self.assertTrue(dict_like(self.misc))

    def test_generator(self):
        keys = {'name', 'age'}
        self.assertEqual(keys, self.get_keys(self.base))
        self.assertEqual(keys, self.get_keys(self.misc))

    def test_get_val(self):
        self.check_get(self.base, 'name', 'Alice')
        self.check_get(self.base, 'blah', None)
        self.check_get(self.misc, 'name', 'Alice')
        self.check_get(self.misc, 'blah', None)

    def test_set_val(self):
        set_value(self.base, 'name', 'Bob')
        set_value(self.base, 'city', 'HK')
        self.assertEqual(self.base, {
            'name': 'Bob',
            'age': 13,
            'city': 'HK',
        })
        set_value(self.misc, 'name', 'Bob')
        set_value(self.misc, 'city', 'HK')
        self.assertEqual(self.misc, [
            ('name', 'Bob'),
            ('age', 13),
            ('city', 'HK'),
        ])
    def test_update(self):
        self.base['data'] = None
        self.base['dict'] = {'type': 'dict'}
        self.base['misc'] = [('type', 'like')]
        #
        self.misc.append(('data', None))
        self.misc.append(('dict', {'type': 'dict'}))
        self.misc.append(('misc', [('type', 'like')]))
        #
        extra_dict = [
            ('type', 'DICT'),
            ('more', True),
        ]
        extra_misc = {
            'type': 'LIKE',
            'more': False,
        }
        extra = [
            ('gender', 'Male'),
            ('name', 'Bob'),
            ('data', {}),
            ('dict', extra_dict),
            ('misc', extra_misc),
        ]
        update_recursive(self.base, extra)
        update_recursive(self.misc, extra)
        #
        self.assertEqual(self.base, {
            'name': 'Bob',
            'age': 13,
            'gender': 'Male',
            'data': {},
            'dict': {
                'type': 'DICT',
                'more': True,
            },
            'misc': [
                ('type', 'LIKE'),
                ('more', False),
            ],
        })
        self.assertEqual(self.misc, [
            ('name', 'Bob'),
            ('age', 13),
            ('data', {}),
            ('dict', {
                'type': 'DICT',
                'more': True,
            }),
            ('misc', [
                ('type', 'LIKE'),
                ('more', False),
            ]),
            ('gender', 'Male'),
        ])
