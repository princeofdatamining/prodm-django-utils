
class DummyObject(object):

    def __init__(self, **kwargs):
        for attr, data in kwargs.items():
            setattr(self, attr, data)

class DictObject(dict):

    def __getattr__(self, key):
        if key in self:
            return self.get(key)
        raise AttributeError

    def __setattr__(self, key, value):
        self[key] = value

def getattr_recursive(obj, attrs, default=None):
    '''
        getattr_recursive(boy, 'parent.name', 'Unknown')
    '''
    if not isinstance(attrs, (tuple, list)):
        attrs = attrs.split('.')
    for attr in attrs[:-1]:
        obj = getattr(obj, attr, None)
        if obj is None:
            return default
    return getattr(obj, attrs[-1], default)

def setattr_recursive(obj, attrs, value):
    '''
        setattr_recursive(boy, 'parent.mobile', 'Unknown')
    '''
    if not isinstance(attrs, (tuple, list)):
        attrs = attrs.split('.')
    for attr in attrs[:-1]:
        obj = getattr(obj, attr, None)
        if obj is None:
            return
    setattr(obj, attrs[-1], value)

empty = object()

class CachedFunction(object):

    def __init__(self, func, *args, **kwargs):
        self.__func = func
        self.__args = args
        self.__kw = kwargs
        self.__data = empty
        
    def __call__(self):
        if self.__data is empty:
            self.__data = self.__func(*self.__args, **self.__kw)
        return self.__data
