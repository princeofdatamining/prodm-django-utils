from django import template
from django.template.base import Node, render_value_in_context, kwarg_re
from django.db.models.base import ModelBase
from django.db.models.options import Options
from django.contrib.admin.options import BaseModelAdmin
from django.core.urlresolvers import reverse

register = template.Library()

import functools

class FormatNode(Node):

    def __init__(self, format_string, args, kwargs, asvar=None, **options):
        self.format_string = format_string
        self.args = args
        self.kwargs = kwargs
        self.asvar = asvar
        self.options = options

    def render_admin_url(self, context, opts, args, kwargs):
        if isinstance(opts, ModelBase):
            opts = opts._meta
        if isinstance(opts, BaseModelAdmin):
            opts = opts.opts
        if isinstance(opts, Options):
            view_name = 'admin:%s_%s_%s' % (opts.app_label, opts.model_name, args[0])
            return reverse(view_name, args=args[1:], kwargs=kwargs)
        return ''

    def render_format(self, context, format_string, args, kwargs):
        if self.options.get('_format', True):
            return format_string.format(*args, **kwargs)
        elif kwargs:
            return format_string % kwargs
        else:
            return format_string % tuple(args)

    def render(self, context):
        format_string = self.format_string.resolve(context)
        args = [arg.resolve(context) for arg in self.args]
        kwargs = {
            k:v.resolve(context) for k, v in self.kwargs.items()
        }
        # print('`', format_string, '`', args, kwargs)
        if '_admin_url' in self.options:
            func = self.render_admin_url
        else:
            func = self.render_format
        result = func(context, format_string, args, kwargs)
        #
        if self.asvar:
            context[self.asvar] = result
            return ''
        return result

def do_format(parser, token, **options):
    bits = token.split_contents()
    format_string = parser.compile_filter(bits[1])
    bits = bits[2:]
    asvar = None
    if len(bits) >= 2 and bits[-2] == 'as':
        asvar = bits[-1]
        bits = bits[:-2]
    args = []
    kwargs = {}
    for bit in bits:
        match = kwarg_re.match(bit)
        name, value = match.groups()
        if name:
            kwargs[name] = parser.compile_filter(value)
        else:
            args.append(parser.compile_filter(value))
    return FormatNode(format_string, args, kwargs, asvar=asvar, **options)
register.tag(name='format')(functools.partial(do_format, _format=True))
register.tag(name='format_divmod')(functools.partial(do_format, _format=False))
register.tag(name='admin_url')(functools.partial(do_format, _admin_url=True))
