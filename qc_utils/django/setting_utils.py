from django.conf import settings
from qc_utils.python.objutils import DictObject

def convert_dict(value):
    if isinstance(value, dict):
        dummy = DictObject()
        for key, val in value.items():
            dummy[key] = convert_dict(val)
        value = dummy
    return value

class Settings(object):

    def __init__(self, name):
        self._conf = getattr(settings, name)
        self._cache = {}

    def __getattr__(self, name):
        if name.startswith('_'):
            return super(Settings, self).__getattr_(name)
        if name in self._cache:
            return self._cache[name]
        self._cache[name] = value = convert_dict( self._conf.get(name) )
        setattr(self, name, value)
        return value
