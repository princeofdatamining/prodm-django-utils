
class DisableCsrfMiddleware(object):

    def process_request(self, request):
        setattr(request, 'csrf_processing_done', True)
