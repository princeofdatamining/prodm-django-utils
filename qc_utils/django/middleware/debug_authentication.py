from django.utils.functional import cached_property
from django.conf import settings

from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.backends import ModelBackend

### Notice: Don't add this `Backend` to `settings.AUTHENTICATION_BACKENDS`.
class GodModelBackend(ModelBackend):

    def authenticate(self, **kwargs):
        UserModel = get_user_model()
        return UserModel.objects.get(**kwargs)

# 指定用户 作为 当前登录用户
class _GodAuthentication(object):
    """
    in settings.py, set:
        GOD_MODE = '_god_as'

    in query_string:
        _god_as=1000 # authenticate with pk=1000
        _god_as=username:who # authenticate with username=who
        _god_as=value&_god_field=mobile # authenticate with mobile=who
    """
    @cached_property
    def god_mode(self):
        return getattr(settings, 'GOD_MODE', None) or settings.LOCAL_SETTINGS.get('GOD_MODE')

    def backdoor(self, request):
        god_user_param = self.god_mode
        if not god_user_param:
            return 
        god_user_id = request.GET.get(god_user_param)
        if not god_user_id:
            return 
        if god_user_id.lower() in ['none', '-1', '0', 'false']:
            logout(request)
            return 
        god_user_field, _, god_user_id = god_user_id.rpartition(':')
        if not god_user_field:
            god_user_field = request.GET.get('_god_field', 'pk')
        if not god_user_field:
            return 
        ###
        if god_user_field == 'token':
            return self.set_token(request, god_user_id)
        user = GodModelBackend().authenticate(**{god_user_field: god_user_id})
        user.backend = 'qc_utils.django.middleware.debug_authentication.GodModelBackend'
        return user

    def set_token(self, request, token):
        request.META['HTTP_AUTHORIZATION'] = 'Token {}'.format(token)

# 身份验证模式： django.contrib.auth : session or cookie
class GodSessionAuthenticationMiddleware(_GodAuthentication):

    def process_request(self, request):
        user = self.backdoor(request)
        if not user or not user.pk: return 
        login(request, user)

GodAuthenticationMiddleware = GodSessionAuthenticationMiddleware

# 身份验证模式： rest_framework.authtoken : Headers['AUTHORIZATION']
class GodTokenAuthenticationMiddleware(_GodAuthentication):

    def process_request(self, request):
        user = self.backdoor(request)
        if not user or not user.pk: return 
        from rest_framework.authtoken.models import Token
        token, _ = Token.objects.get_or_create(user=user)
        self.set_token(request, token)
