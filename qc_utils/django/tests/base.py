from django.test import TestCase

class AuthMixin(object):

    @property
    def Users(self):
        if not hasattr(self, '_Users'):
            from ..modelutils import import_user_model
            vars = {}
            import_user_model(vars)
            self._Users = vars['Users']
        return self._Users

    def force_user(self, username, **kwargs):
        password = kwargs.pop('password', None)
        u, _ = self.Users.get_or_create(defaults=kwargs, username=username)
        if password:
            u.set_password(password)
            u.save()
        try:
            from rest_auth.restful.utils import obtain_auth_token
        except:
            pass
        else:
            u.token = obtain_auth_token(u)
        return u
