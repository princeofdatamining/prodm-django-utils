from unittest import TestCase
from django.conf import settings
from ..setting_utils import Settings

class TestSetting(TestCase):

    def setUp(self):
        self.cnf = Settings('LOGGING')
        self.raw = settings.LOGGING

    def test(self):
        self.assertEqual(self.raw['version'], self.cnf.version)
        self.assertEqual(self.raw['handlers']['console']['formatter'], self.cnf.handlers.console.formatter)
