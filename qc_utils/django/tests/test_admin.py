from .base import TestCase

from ..adminutils import *

class TestAdminUtils(TestCase):

    def test_url(self):
        from django.contrib.auth.models import Group
        self.check_url('/admin/auth/group/', Group)
        self.check_url('/admin/auth/group/', Group, 'changelist')
        self.check_url('/admin/auth/group/add/?active=1', Group, 'add', active=1)
        #
        g = Group(pk=2)
        self.check_url('/admin/auth/group/2/change/', g)
        self.check_url('/admin/auth/group/2/change/', g, 'change')
        self.check_url('/admin/auth/group/2/delete/', g, 'delete')
        self.check_url('/admin/auth/group/', g, 'changelist')
        self.check_url('/admin/auth/group/add/', g, 'add')
        #
        self.check_url('/admin/auth/group/2/change/', g, model_class=Group)

    def check_url(self, expect, *args, **kwargs):
        self.assertIn(expect, admin_url(*args, **kwargs))
