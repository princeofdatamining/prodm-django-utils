from django.contrib.auth.forms import ReadOnlyPasswordHashWidget
from django.forms.utils import flatatt
from django.utils.html import format_html

''' 修改｀用户｀信息时，密码部分的显示太复杂：
    算法: pbkdf2_sha256 迭代次数: 24000 盐: b4A4R8****** 哈希: y8MBHb**************************************
    原始密码不存储，所以没有办法看到这个用户的密码，但可以使用 这个表单 来更改密码 。
'''

def render(self, name, value, attrs):
    final_attrs = self.build_attrs(attrs)
    return format_html("<div{}>{}</div>", flatatt(final_attrs), '')
ReadOnlyPasswordHashWidget.render = render
