from django.contrib.sites.models import Site
from django.contrib.admin import site
from django.conf import settings

custom = getattr(settings, 'LOCAL_SETTINGS', {})

name = getattr(settings, 'SITE_NAME', None) or custom.get('SITE_NAME')

title = getattr(settings, 'SITE_TITLE', None) or custom.get('SITE_TITLE') or name
if title: site.site_title = title

header = getattr(settings, 'SITE_HEADER', None) or custom.get('SITE_HEADER') or name
if header: site.site_header = header

# 更新 django_site 表
def force_sites():
    siteid = getattr(settings, 'SITE_ID', None) or custom.get('SITE_ID')
    domain = getattr(settings, 'SITE_DOMAIN', None) or custom.get('SITE_DOMAIN') or 'localhost:8000'

    values = {}
    domain and values.setdefault('domain', domain)
    name and values.setdefault('name', name)

    if siteid:
        Site.objects.update_or_create(defaults=values, pk=siteid)
    elif name:
        Site.objects.create(**values)
# force_sites()
