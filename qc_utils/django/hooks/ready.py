from django.conf import settings
from importlib import import_module

hooks = getattr(settings, 'IMPORT_HOOKS', None)
if hooks:
    for module in hooks:
        try:
            import_module(module)
        except Exception as e:
            print("import fail: `{}` `{}`!".format(module, e))
        else:
            pass
