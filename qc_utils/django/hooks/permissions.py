from django.db.models import Model, ObjectDoesNotExist
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.conf import settings
from django.apps import apps
from django.utils.translation import ugettext_lazy, pgettext_lazy

GROUP_ALL = pgettext_lazy('Auth Group', 'All Permissions')

""" 禁止 django migrate 自动生成 auth_permission
    Why? 自动生成的 权限名 带有英文,用户体验不好
# """
from django.db.models.signals import post_migrate
post_migrate.disconnect(dispatch_uid='django.contrib.auth.management.create_permissions')

meta_list, meta_dict = [], {}
def load_installed_models():
    from importlib import import_module
    for app in settings.INSTALLED_APPS:
        mod_path = app + '.models'
        # print('load ', mod_path)
        try:
            module = import_module(mod_path)
        except:
            continue
        for model in dir(module):
            model = getattr(module, model, None)
            if not isinstance(model, type) or not issubclass(model, Model):
                continue
            if model.__module__ != mod_path:
                continue
            meta = getattr(model, "_meta", None)
            if not meta or getattr(meta, "abstract", None) or getattr(meta, "swapped", None):
                continue
            identity = '{}.{}'.format(meta.app_label, meta.model_name)
            if identity in meta_dict:
                continue
            meta_list.append(meta)
            meta_dict[identity] = meta
            print('\t', identity)
    print('loaded INSTALLED_APPS')

# 根据 alias（`auth.group`）获取 Model._meta
def get_model_meta(alias):
    return meta_dict[alias]

USER = settings.AUTH_USER_MODEL
def setup_users():
    _globals = globals()
    if 'Users' not in _globals:
        _globals['User'] = user_model = apps.get_model(USER)
        _globals['Users'] = user_model.objects
# setup_users()

# 载入原先的权限列表
origin_permissions = {}
def load_permissions():
    origin_permissions.update({
        '{}.{}.{}'.format(
            p.content_type.app_label,
            p.content_type.model,
            p.codename,
        ): p.pk 
        for p in Permission.objects.all()
    })
    print('Origin permissions: {}', len(origin_permissions))

# 移除已废弃的权限
def save_permissions():
    deprecated = []
    for key, pk in origin_permissions.items():
        if pk :
            deprecated.append(pk)
            print('Deprecated permission: {}'.format(key))
    if not deprecated:
        print('No deprecated permissions.')
        return 
    for g in Group.objects.all():
        g.permissions.filter(id__in=deprecated).delete()
    for u in Users.filter(is_staff=True):
        u.user_permissions.filter(id__in=deprecated).delete()
    Permission.objects.filter(id__in=deprecated).delete()

def set_model_permissions(meta, actions=['add', 'change', 'delete']):
    app_label, model_name, verbose_name = meta.app_label, meta.model_name, meta.verbose_name
    ct, _ = ContentType.objects.get_or_create(app_label=app_label, model=model_name)
    perms = []
    for action in actions:
        name = '{} {}'.format(pgettext_lazy('Codename', 'Can '+action), ugettext_lazy(verbose_name))
        codename = '{}_{}'.format(action, model_name)
        perm, ok = Permission.objects.update_or_create(defaults=dict(
            name=name,
        ), content_type=ct, codename=codename)
        origin_permissions.pop('{}.{}.{}'.format(app_label, model_name, codename), None)
        perms.append(perm)
    return perms

# 'auth.user.add|change|delete' ==> Permissions
def codename_to_permissions(codename):
    meta, _, action = codename.lower().rpartition('.')
    # app_label, _, model_name = meta.partition('.')
    return set_model_permissions(get_model_meta(meta), action.split('|'))


''' set_group_permissions('Group name',
    'auth.user.add|change|delete',
    'auth.group.add|change|delete',
    ...
)
'''
def set_group_permissions(group, *codenames):
    if not isinstance(group, Group):
        group, _ = Group.objects.get_or_create(name=group)
    #
    pks, permissions = [], {}
    for perm in group.permissions.all():
        pks.append(perm.pk)
        permissions[perm.pk] = perm
    #
    for codename in codenames:
        for perm in codename_to_permissions(codename):
            if perm.pk in pks:
                pks.remove(perm.pk)
            else:
                group.permissions.add(perm)
    if pks:
        print('Group `{}` drop {} permissions.'.format(group.name, len(pks)))
        group.permissions.filter(pk__in=pks).delete()
    # group.save()
    return group

def set_user_groups(user, *groups):
    for group in groups:
        user.groups.filter(pk=group.pk) or user.groups.add(group)
    user.save()
    return user

def set_site(id, **kwargs):
    Site.objects.update_or_create(defaults=kwargs, pk=id)
