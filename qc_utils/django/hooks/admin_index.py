from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.admin import site
from django.conf import settings

# 让 /admin/ 重定向到指定页面
path = getattr(settings, 'ADMIN_INDEX_URL', '')

def admin_index(request):
    url = reverse(path)
    return HttpResponseRedirect(url)

if path: site.index = admin_index
