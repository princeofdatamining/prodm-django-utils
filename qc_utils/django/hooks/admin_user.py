from django.contrib.auth import admin
from django.conf import settings

'''
    FIELDS_DISPLAY_USER = ('username', 'email', 'roles',)

    FIELDSETS_STAFF = [
        (
            (None, ('username', 'password')),
            (_('Personal info'), ('name', 'email',)),
        ), (
            (None, ('username', 'password1', 'password2')),
            (_('Personal info'), ('name', 'email',)),
            (_('Permissions'), ('is_active', 'is_staff', 'groups')),
        )
    ]
'''

class BaseUserAdmin(admin.UserAdmin):

    FIELDSETS = ''
    DISPLAY = 'FIELDS_DISPLAY_USER'

    def make_fieldsets(self, sets, add=False):
        return (
            (group, dict(fields=fields, classes=('wide',))) if add else (group, dict(fields=fields))
            for group, fields in sets[add]
        )
    def get_fieldsets(self, request, obj=None):
        if self.FIELDSETS:
            fields = getattr(settings, self.FIELDSETS, None)
            add = obj is None
            if fields and fields[add]:
                return self.make_fieldsets(fields, add)
        return super(BaseUserAdmin, self).get_fieldsets(request, obj)

    def get_list_display(self, request):
        if self.DISPLAY:
            fields = getattr(settings, self.DISPLAY, None)
            if fields: return fields
        return super(BaseUserAdmin, self).get_list_display(request)

    def get_queryset(self, request):
        qs = super(BaseUserAdmin, self).get_queryset(request)
        qs = self.filter_queryset(request, qs)
        return qs

    def filter_queryset(self, request, qs):
        return qs.filter(is_superuser=False, is_staff=False)

class UserAdmin(BaseUserAdmin):

    FIELDSETS = 'FIELDSETS_USER'

class StaffAdmin(BaseUserAdmin):

    FIELDSETS = 'FIELDSETS_STAFF'

    def filter_queryset(self, request, qs):
        return qs.filter(is_superuser=False, is_staff=True)
