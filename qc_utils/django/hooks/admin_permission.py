from django.contrib.auth.models import Permission, Group, AbstractUser
from django.contrib.auth.admin import GroupAdmin
from django.conf import settings
from django.utils.translation import ugettext_lazy, pgettext_lazy

# 权限的默认显示： `auth | User | add`不直观
Permission.__str__ = lambda self: self.name

group_verbose = getattr(settings, 'GROUP_VERBOSE_NAME', '')
if group_verbose:
    if isinstance(group_verbose, (list, tuple)):
        Group._meta.verbose_name , Group._meta.verbose_name_plural = group_verbose
    else:
        Group._meta.verbose_name = Group._meta.verbose_name_plural = group_verbose

# 显示staff user的groups
def roles(self):
    return ','.join([g.name for g in self.groups.all()])
roles.short_description = ugettext_lazy('Permissions')
AbstractUser.roles = roles
