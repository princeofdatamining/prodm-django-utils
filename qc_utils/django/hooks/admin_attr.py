from django.conf import settings

# 修改 model_admin 的默认属性
attrs = getattr(settings, 'MODEL_ADMIN_ATTRS', {})

# 修改所有实例，还是基类
if not getattr(settings, 'MODEL_ADMIN_ATTRS_FOR_ALL', None):
    from django.contrib.admin.options import ModelAdmin
    for attr, value in attrs.items():
        setattr(ModelAdmin, attr, value)
else:
    from django.contrib.admin import site
    for model, admin in site._registry.items():
        for attr, value in attrs.items():
            setattr(admin, attr, value)
