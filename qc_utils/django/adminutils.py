from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.db.models import Model, Max

from django.http import JsonResponse
from django.conf.urls import url, include
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy, ungettext_lazy, pgettext_lazy, npgettext_lazy

from urllib import parse
from collections import OrderedDict

from qc_utils.django import time as timeutils
from qc_utils.django import charts

''' Model/Instance 直接生成 admin url
    admin_url(User, **) ==> '/admin/auth/user/'
    admin_url(user, **) ==> '/admin/auth/user/pk/change/'
    admin_url(xx, 'delete', **) ==> '/admin/APP/XXX/pk/delete/'
    admin_url(user, model_class='ProxyUser', **) ==> '/admin/app/proxyuser/pk/change/'
    #
    params: dict_like => ?key=value&name=xx
'''
def admin_url(model_or_instance, action='', model_class=None, **params):
    if not action:
        action = 'change' if isinstance(model_or_instance, Model) else 'changelist'
    args = ()
    if action in ('change', 'delete'):
        args = (model_or_instance.pk,)
    if not model_class:
        model_class = model_or_instance
    info = model_class._meta.app_label, model_class._meta.model_name, action
    query_string = parse.urlencode(params)
    url = ''.join([
        reverse('admin:%s_%s_%s' % info, args=args),
        '?' if query_string else '',
        query_string,
    ])
    return url

''' 有时候，只需要简单更改一下某个属性就要进入｀changeform｀太麻烦
    delete=dict( values=dict(deleted=True) ) # /.../?delete=[any]
    toggle=dict( values=dict(deleted=not models.F('deleted')) ) # /.../?toggle=[any]

    def change_view(self, request, object_id, **kwargs):
        if change_redirect(self, request, object_id,
            delete=dict( values=dict(deleted=True) ),
        ):
            return self.response_post_save_add(request, None)
        super(..., self).change_view(request, object_id, **kwargs)
'''
def change_directly(model_admin, request, pk, **options):
    for param, handler in options.items():
        filter = handler.get('filter')
        if any((
            not filter and param in request.GET,
            filter == request.GET.get(param, object),
            callable(filter) and filter(request, param=param),
        )):
            values = handler.get('values', {})
            model_admin.get_queryset(request).filter(pk=pk).update(**values)
            return True

#

class EChartsWPAdminMedia:
    js = (
        'admin/js/jquery.min.js',
        'admin/js/moment.min.js',
        'admin/js/bootstrap.min.js',
        'admin/js/echarts.min.js',
        'admin/js/bootstrap-datetimepicker.min.js',
    )
    css = {
        'all': (
            'admin/css/bootstrap.min.css',
            'admin/css/bootstrap-datetimepicker.min.css',
        )
    }

class EChartsBootstrapMedia:
    js = (
        'admin/js/moment.min.js',
        'admin/js/echarts.min.js',
        'admin/js/bootstrap-datetimepicker.min.js',
        'admin/js/bootstrap-datetimepicker.locale.js',
    )
    css = {
        'all': (
            'admin/css/bootstrap-datetimepicker.min.css',
        )
    }

class EChartsModelAdmin(admin.ModelAdmin):

    change_list_template = 'admin/charts/bootstrap.html'

    Media = EChartsBootstrapMedia

    def get_queryset(self, request):
        return self.filter_queryset(super(EChartsModelAdmin, self).get_queryset(request), request)

    def filter_queryset(self, qs, request):
        return qs

    def get_legend_filters(self, legend):
        if not hasattr(self, 'FILTERS'): self.FILTERS = {}
        return self.FILTERS.get(legend, {})

    defaulttime_func = timeutils.timezone.now
    time_postfunc = timeutils.tz.utc
    time_prefunc = timeutils.tz.localtime
    TIME_FIELD = 'create_time'

    def charts_ajax(self, request):
        data = charts.build_charts(
            self.get_queryset(request),
            request.GET.get('from'), request.GET.get('to'),
            step=request.GET.get('step'),
            default=self.__class__.defaulttime_func,
            pre_func=self.__class__.time_prefunc,
            post_func=self.__class__.time_postfunc,
            time_field=self.TIME_FIELD,
            legends=self.LEGEND,
            filter_set=self.get_legend_filters,
        )
        return JsonResponse(charts.build_echarts(
            data,
            self.LEGEND,
            stack_mapping=self.STACKED,
            width=self.WIDTH,
            make_line_legend=self.MARK_LINE_LEGEND,
            make_line_style=self.MARK_LINE,
        ))

    def recent_ajax(self, request):
        return JsonResponse(charts.recent_data(
            self.get_queryset(request),
            default=self.__class__.defaulttime_func,
            pre_func=self.__class__.time_prefunc,
            post_func=self.__class__.time_postfunc,
            time_field=self.TIME_FIELD, 
        ))

    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.model_name
        return [
            url(r'^recent/$', self.recent_ajax, name='%s_%s_recent' % info),
            url(r'^charts/$', self.charts_ajax, name='%s_%s_charts' % info),
        ] + super(EChartsModelAdmin, self).get_urls()

    LEGEND = ['count']
    STACKED = {
    }
    MARK_LINE_LEGEND = None
    MARK_LINE = {
        'lineStyle': {
            'normal': {
                'type': 'dashed',
            },
        },
        'data' : [
            [{'type' : 'min'}, {'type' : 'max'}],
        ],
    }
    WIDTH = {
    }

    ''' 
        MARK_LINE_LEGEND = None
        LEGEND = ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎','百度','谷歌','必应','其他']
        STACKED = {
            '邮件营销': '广告',
            '联盟广告': '广告',
            '视频广告': '广告',
            '百度':'搜索引擎',
            '谷歌':'搜索引擎',
            '必应':'搜索引擎',
            '其他':'搜索引擎',
        }
        MARK_LINE = {
            'lineStyle': {
                'normal': {
                    'type': 'dashed',
                },
            },
            'data' : [
                [{'type' : 'min'}, {'type' : 'max'},],
            ],
        }
        WIDTH = {
            '搜索引擎!': 5,
        }
        make_options({
            'titles': ['周一','周二','周三','周四','周五','周六','周日'],
            '直接访问': [320, 332, 301, 334, 390, 330, 320],
            '邮件营销': [120, 132, 101, 134, 90, 230, 210],
            '联盟广告': [220, 182, 191, 234, 290, 330, 310],
            '视频广告': [150, 232, 201, 154, 190, 330, 410],
            '搜索引擎': [862, 1018, 964, 1026, 1679, 1600, 1570],
            '百度': [620, 732, 701, 734, 1090, 1130, 1120],
            '谷歌': [120, 132, 101, 134, 290, 230, 220],
            '必应': [60, 72, 71, 74, 190, 130, 110],
            '其他': [62, 82, 91, 84, 109, 110, 120],
        })
    # '''
    pass

#
