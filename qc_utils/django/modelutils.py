from django.apps import apps
from django.conf import settings
from django.db.models import Model
from django.contrib.contenttypes.models import ContentType

# 调用 modelutils.import_user_model(globals()) 即可给当前｀module｀引入｀User Model｀
def import_user_model(vars):
    if 'Users' in vars:
        return 
    model = apps.get_model(settings.AUTH_USER_MODEL)
    vars['User'], vars['Users'] = model, model.objects

# 在 ContentType、ContentType.id、ModelClass、ModelAlias 间互相转换并缓存结果
class content_type_cache(object):

    cached = {}

    @classmethod
    def add_cache(cls, ct, model=None, namespace=(), alias=''):
        model = ct.model_class()
        if not namespace: namespace = ct.natural_key()
        if not alias: alias = '.'.join(ct.natural_key())
        cls.cached[model] = cls.cached[alias] = cls.cached[namespace] = cls.cached[ct.pk] = n = {
            'content_type': ct,
            'content_type_id': ct.pk,
            'model_class': model,
            'model_name': '.'.join(ct.natural_key()),
            'tuple_name': ct.natural_key(),
        }
        return n

    @classmethod
    def force(cls, v):
        if isinstance(v, ContentType):
            if v.pk in cls.cached:
                return cls.cached[v.pk]
            return cls.add_cache(v)
        if isinstance(v, int):
            if v in cls.cached:
                return cls.cached[v]
            return cls.add_cache(ContentType.objects.get_for_id(v))
        if isinstance(v, str):
            v = v.lower()
            if v in cls.cached: return cls.cached[v]
            return cls.add_cache(ContentType.objects.get_by_natural_key(*v.split('.')), alias=v)
        if isinstance(v, tuple):
            if v in cls.cached:
                return cls.cached[v]
            return cls.add_cache(ContentType.objects.get_by_natural_key(*v), namespace=v)
        if isinstance(v, Model): v = v.__class__
        if isinstance(v, type) and issubclass(v, Model):
            if v in cls.cached:
                return cls.cached[v]
            return cls.add_cache(ContentType.objects.get_for_model(v), model=v)

    @classmethod
    def get_what(cls, model, attr):
        c = cls.force(model)
        return c.get(attr) if c else None

    @classmethod
    def get_content_type(cls, model):
        return cls.get_what(model, 'content_type')

    @classmethod
    def get_content_type_id(cls, model):
        return cls.get_what(model, 'content_type_id')

    @classmethod
    def get_model_class(cls, model):
        return cls.get_what(model, 'model_class')

    @classmethod
    def get_model_name(cls, model):
        return cls.get_what(model, 'model_name')

    @classmethod
    def get_tuple_name(cls, model):
        return cls.get_what(model, 'tuple_name')

get_content_type = content_type_cache.get_content_type
get_content_type_id = content_type_cache.get_content_type_id
get_model_class = content_type_cache.get_model_class
get_model_name = content_type_cache.get_model_name
get_tuple_name = content_type_cache.get_tuple_name
