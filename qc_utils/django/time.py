from django.utils import timezone, dateparse
# from django.utils.timesince import TIMESINCE_CHUNKS
from django.templatetags import tz

from datetime import date, time, datetime, timedelta
import functools
import random
import math
import numbers
import operator

DURATION_MINUTE = timedelta(minutes=1)
DURATION_HOUR = timedelta(hours=1)
DURATION_DAY = timedelta(days=1)
DURATION_WEEK = 7 * DURATION_DAY

def convert_datetime(t, converter=None):
    if not converter:
        return t
    if callable(converter):
        return converter(t)
    if isinstance(converter, str):
        converter = converter.lower()
        if converter in ['localtime']:
            return tz.localtime(t)
        if converter in ['utc']:
            return tz.utc(t)
    return t

def force_aware(value, tzinfo=None):
    if timezone.is_aware(value):
        return value
    return timezone.make_aware(value, tzinfo or timezone.get_default_timezone())

'''
    parse_duration(timedelta(...), **timedelta_kwargs)
    parse_duration(2, **timedelta_kwargs)
    parse_duration('DD HH:MM:SS.uuuuuu')
    parse_duration('P4DT1H15M20S')
    parse_duration(**timedelta_kwargs)
'''
def parse_duration(D=None, **kwargs):
    if isinstance(D, timedelta):
        return D + timedelta(**kwargs)
    if isinstance(D, numbers.Number):
        kwargs['days'] = D
        return timedelta(**kwargs)
    if not D:
        return timedelta(**kwargs)
    if isinstance(D, str):
        return dateparse.parse_duration(D, **kwargs)
    if isinstance(D, dict):
        return parse_duration(**D)

def parse_datetime(T=None, default=timezone.now):
    if isinstance(T, (date, time, datetime)):
        return T
    if isinstance(T, dict):
        return default() + parse_duration(**T)
    if isinstance(T, str):
        if dateparse.date_re.match(T):
            parsed = dateparse.parse_date(T)
            return datetime(parsed.year, parsed.month, parsed.day)
        if dateparse.datetime_re.match(T):
            return dateparse.parse_datetime(T)
    return default() + parse_duration(T)

supported_steps = ('year', 'month', 'week', 'day', 'hour', 'minute')

def validate_step(step, default=None):
    return step if step in supported_steps else default

def truncate_datetime(t=None, step='day',
    default=timezone.now, pre_func=None, post_func=None,
):
    t = parse_datetime(t, default)
    t = convert_datetime(t, pre_func)
    # truncate days
    days = 0
    if step == 'year':
        t = t.replace(month=1, day=1)
    elif step == 'month':
        t = t.replace(day=1)
    elif step == 'week':
        t -= t.weekday() * DURATION_DAY
    # truncate H/M/S
    t = t.replace(second=0, microsecond=0)
    if step not in ['minute']:
        t = t.replace(minute=0)
    if step not in ['minute', 'hour']:
        t = t.replace(hour=0)
    #
    return convert_datetime(t, post_func)

def datetime_range(t=None, step='day',
    default=timezone.now, pre_func=None, post_func=None,
):
    t = parse_datetime(t, default)
    t = convert_datetime(t, pre_func)
    left = truncate_datetime(t, step)
    #
    if step == 'year':
        right = left.replace(year=left.year+1)
    elif step == 'month':
        right = left.replace(month=1 if left.month >= 12 else left.month+1)
    elif step == 'week':
        right = left + DURATION_WEEK
    elif step == 'day':
        right = left + DURATION_DAY
    elif step == 'hour':
        right = left + DURATION_HOUR
    elif step == 'minute':
        right = left + DURATION_MINUTE
    else:
        right = left
    #
    return (
        convert_datetime(left, post_func),
        convert_datetime(t, post_func),
        convert_datetime(right, post_func),
    )

def try_datetime_offset(t, year=None, month=None):
    if year:
        y = t.year + year
        try:
            return t.replace(year=y)
        except:
            # Only failed when yyyy.02.29
            return t.replace(year=y, month=t.month+1, day=1)
    elif month:
        m = t.month + month
        if m > 12:
            # yyyy-12-dd -> yyyz-01-dd always successful
            return t.replace(year=t.year+1, month=1)
        elif m < 1:
            # yyyy-01-dd -> yyyx-12-dd always successful
            return t.replace(year=t.year-1, month=12)
        try:
            return t.replace(month=m)
        except:
            pass
        if month > 0:
            # yyyy-01-31 -> yyyy-03-01
            return t.replace(month=m+1, day=1)
        else:
            # yyyy-12-31 -> yyyy-11-30
            return t.replace(day=1) - DURATION_DAY
    return t

def datetime_offset(t=None, step='day', op='+',
    default=timezone.now, pre_func=None, post_func=None,
):
    t = parse_datetime(t, default)
    t = convert_datetime(t, pre_func)
    #
    delta = -1 if op == '-' else +1
    if step == 'year':
        t = try_datetime_offset(t, year=delta)
    elif step == 'month':
        t = try_datetime_offset(t, month=delta)
    elif step == 'week':
        t += delta * DURATION_WEEK
    elif step == 'day':
        t += delta * DURATION_DAY
    elif step == 'hour':
        t += delta * DURATION_HOUR
    elif step == 'minute':
        t += delta * DURATION_MINUTE
    else:
        t = t
    return convert_datetime(t, post_func)
