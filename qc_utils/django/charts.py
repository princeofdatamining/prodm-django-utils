from django.utils import timezone
from django.templatetags import tz
from django.utils.translation import ugettext_lazy, ungettext_lazy, pgettext_lazy, npgettext_lazy
from collections import OrderedDict

from qc_utils.django.time import convert_datetime, parse_datetime, datetime_range, DURATION_DAY, validate_step

def aggregate(
    queryset,
    date_from, date_to,
    time_field='create_time',
    args=(),
    **filters
):
    date_from and filters.setdefault(time_field + '__gte', date_from)
    date_to and filters.setdefault(time_field + '__lt', date_to)
    return queryset.filter(*args, **filters).count()

def recent_data(
    queryset,
    default=timezone.now, pre_func=tz.localtime, post_func=tz.utc,
    **kwargs
):
    today, _, tomorrow = datetime_range(step='day', default=default, pre_func=pre_func, post_func=post_func)
    yesterday = today - DURATION_DAY
    return dict(
        today=aggregate(queryset, today, tomorrow, **kwargs),
        yesterday=aggregate(queryset, yesterday, today, **kwargs),
        all=aggregate(queryset, None, None, **kwargs),
    )

def build_charts(queryset,
    date_from, date_to,
    step='month',
    default=timezone.now, pre_func=tz.localtime, post_func=tz.utc,
    date_format=None,
    time_field='create_time',
    legends=('count',),
    filter_set=None,
):
    step = validate_step(step, 'month')
    if not date_format:
        date_format = {
            'hour': '%Hh',
            'day': '%m%d',
            'week': '%m%d',
            'month': '%b',
            'year': '%Y',
        }.get(step, '%m%d')
    date_range = {
        'hour': 'day',
        'day': 'month',
        'week': 'month',
        'month': 'year',
        'year': 'year',
    }.get(step, 'month')
    if date_from and date_to:
        date_from = convert_datetime(parse_datetime(date_from, default), post_func)
        date_to = convert_datetime(parse_datetime(date_to, default), post_func)
    elif date_from and not date_to:
        date_from = convert_datetime(parse_datetime(date_from, default), post_func)
        _, _, date_to = datetime_range(date_from, date_range, pre_func=pre_func, post_func=post_func)
    else:
        date_to = convert_datetime(parse_datetime(date_to, default), post_func)
        date_from, _, _ = datetime_range(date_to, date_range, pre_func=pre_func, post_func=post_func)
    #
    if callable(filter_set):
        temp = {
            legend: filter_set(legend)
            for legend in legends
        }
        filter_set = temp
    if isinstance(filter_set, dict):
        for legend, value in filter_set.items():
            if callable(value):
                filter_set[legend] = value()
    else:
        filter_set = {}
    #
    results = {legend: [] for legend in legends}
    results['titles'] = titles = []
    while date_from < date_to:
        _, _, date_next = datetime_range(date_from, step,
            pre_func=pre_func, post_func=post_func,
        )
        if date_next > date_to: date_next = date_to
        #
        title = (pre_func(date_from) if pre_func else date_from).strftime(date_format)
        titles.append(str(ugettext_lazy(title)))
        for legend in legends:
            filters = filter_set.get(legend, {})
            if isinstance(filters, dict):
                args = ()
            elif not isinstance(filters, (list, tuple)):
                args, filters = (), {}
            elif isinstance(filters[-1], dict):
                args, filters = filters[:-1], filters[-1]
            else:
                args, filters = filters, {}
            data = aggregate(queryset, date_from, date_next, time_field, args, **filters)
            results[legend].append(data)
        date_from = date_next
    return results

def build_echarts(data, legends, chart_type='bar',
    stack_mapping={},
    width={},
    make_line_legend=None, make_line_style=None,
):
    stacked = {}
    series = []
    for legend in legends:
        cache = OrderedDict()
        cache['name'] = legend
        cache['type'] = chart_type
        stack = stack_mapping.get(legend)
        stack and cache.setdefault('stack', stack)
        cache['data'] = data[legend]
        if legend in width:
            cache['barWidth'] = width[legend]
        elif stack and (stack + '!') in width and stack not in stacked:
            cache['barWidth'] = width[stack + '!']
            stacked[stack] = True
        if make_line_legend == legend:
            cache['markLine'] = make_line_style
        series.append(cache)
    return {
        'tooltip' : {
            'trigger': 'axis',
            'axisPointer' : {
                'type' : 'shadow',
            },
        },
        'legend': {
            'data': legends,
        },
        'grid': {
            'left': '3%',
            'right': '4%',
            'bottom': '3%',
            'containLabel': True,
        },
        'xAxis' : [
            {
                'type' : 'category',
                'data' : data['titles'],
            },
        ],
        'yAxis' : [
            {
                'type' : 'value',
            },
        ],
        'series' : series,
    }
