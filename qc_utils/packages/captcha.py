from django.conf import settings

CAPTCHA_TEST_HASHKEY = getattr(settings, 'CAPTCHA_TEST_HASHKEY', None)

if 'captcha' in settings.INSTALLED_APPS:
    from captcha.models import CaptchaStore, get_safe_now

    def validate_captcha(hashkey, response, exception_class):
        CaptchaStore.remove_expired()
        try:
            c = CaptchaStore.objects.get(hashkey=hashkey, response=response, expiration__gt=get_safe_now())
        except CaptchaStore.DoesNotExist:
            raise exception_class('Invalid CAPTCHA')
        else:
            hashkey == CAPTCHA_TEST_HASHKEY or c.delete()
        return True

else:

    def validate_captcha(*args, **kwargs):
        return True
